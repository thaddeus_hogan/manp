/*
 * MANpWinker.h
 *
 * Created: 9/29/2012 12:14:46 PM
 *  Author: thogan
 */ 

#include "MANpSlaveProtocol.h"

#ifndef MANPWINKER_H_
#define MANPWINKER_H_

#define NV_VAR_WINK_INTENSITY (uint8_t*)0x0002
#define NV_VAR_WINK_FREQ (uint8_t*)0x0003
#define NV_VAR_WINK_DC (uint8_t*)0x0004

#define _20MS_TICKS 250

#define MPWST_WINK 0
#define MPWST_EEPUP 1
#define MPWST_RIGHT 2

void winkerConfigure(void);

// Callback for message receipt
void onMessage(mpmsg* msg);


#endif /* MANPWINKER_H_ */