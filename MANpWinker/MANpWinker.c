/*
 * MANpWinker.c
 *
 * Created: 9/29/2012 11:55:59 AM
 *  Author: thogan
 */ 

#define F_CPU 12000000UL

#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#include "MANpWinker.h"
#include "MANpProtocolData.h"
#include "MANpSlaveProtocol.h"

#define PWM_ON TCCR0A |= (2<<COM0B0); TCCR0B |= (3<<CS00)
#define PWM_OFF TCCR0A &= ~(2<<COM0B0); TCCR0B &= ~(3<<CS00)

#define INT0_FLAG GIFR & (1<<INTF0)
#define INT0_RESET GIFR |= (1<<INTF0)

#define RESET_WINK ticks80us = 0; ticks20ms = 0

uint8_t volatile winkerState = 1;

uint8_t volatile winkIntensity;
uint8_t volatile winkFreq = 6;
uint8_t volatile winkDC = 3;

uint8_t volatile ticks80us = 0;
uint8_t volatile ticks20ms = 0;

int main(void)
{
	// Load parameters from EEPROM
	winkIntensity = eeprom_read_byte(NV_VAR_WINK_INTENSITY);
	winkFreq = eeprom_read_byte(NV_VAR_WINK_FREQ);
	winkDC = eeprom_read_byte(NV_VAR_WINK_DC);
	
	// Configure winker
	winkerConfigure();
	
	BTIM_RESET;
	BTIM_RESET_FLAG;
	
	while(1)
    {
		while (BTIM_NO_FLAG) {
			if (!(MPPIN & (1<<MPBIT))) {
				mpmsg* msg = manpReceiveMessage();
				onMessage(msg);
			}
		}
		
		BTIM_RESET_FLAG;
		ticks80us += 1;
		
		if (ticks80us == _20MS_TICKS) {
			ticks20ms += 1;
			ticks80us = 0;
		}
		
		if (winkerState & (1<<MPWST_WINK)) {
			if (ticks20ms < winkDC) {
				PWM_ON;
			} else {
				PWM_OFF;
			}
			
			if (ticks20ms > winkFreq) {
				ticks20ms = 0;
			}
		} else {
			PWM_OFF;
		}
		
		if (winkerState & (1<<MPWST_EEPUP)) {
			winkerState &= ~(1<<MPWST_EEPUP);
			
			eeprom_write_byte(NV_VAR_WINK_INTENSITY, winkIntensity);
			eeprom_write_byte(NV_VAR_WINK_FREQ, winkFreq);
			eeprom_write_byte(NV_VAR_WINK_DC, winkDC);
		}		
    }
}

void winkerConfigure(void) {
	// Detect jumper configuration for left or right winker
	// Jumper closed will indicate right blinker
	DDRB &= ~(1<<DDB0); // PB0 is input
	PORTB |= (1<<PB0); // Enable pull-up resistor for a clean reading
	_delay_us(100); // Wait for things to stabilize
	
	// Test jumper
	if (PINB & (1<<PB0)) {
		// Reading 1 from internal pull-up, jumper is off, winker is left
		winkerState &= ~(1<<MPWST_RIGHT);
		// We'll leave the pull-up enabled for NC pin
	} else {
		// Reading 0 from stronger pull down, jumper is on, winker is right
		winkerState |= (1<<MPWST_RIGHT);
		PORTB &= ~(1<<PB0); // Disable pull-up to save power
	}
	
	// Wait for master to start
	_delay_ms(250);
	
	// PB2 - Data Input
	MPPORT &= ~(1<<MPBIT);
	MPDDR &= ~(1<<MPBIT);
	
	// PB1 - PWM Output to emitter FET
	DDRB |= (1<<PB1);
	
	TCCR0A |= (3<<WGM00);
	OCR0B = winkIntensity;
	
	// Configure Timer1 for use with MANp, also use for wink timing
	manpConfigTimer1_ATtiny85();
}

// Callback for message receipt
void onMessage(mpmsg* msg) {
	if (msg->id & MP_VAR_MASK) {
		// Variable write
		if (msg->id == VAR_WINK_INTENSITY) {
			winkIntensity = msg->val;
			OCR0B = winkIntensity;
		} else if (msg->id == VAR_WINK_FREQ) {
			winkFreq = msg->val;
			RESET_WINK;
		} else if (msg->id == VAR_WINK_DC) {
			winkDC = msg->val;
			RESET_WINK;
		}
		
		winkerState |= (1<<MPWST_EEPUP);
	} else {
		// State Message
		if ((msg->id == ST_L_WINK_ON && !(winkerState & (1<<MPWST_RIGHT))) || (msg->id == ST_R_WINK_ON && winkerState & (1<<MPWST_RIGHT))) {
			if (!(winkerState & (1<<MPWST_WINK))) {
				RESET_WINK;
			}
			winkerState |= (1<<MPWST_WINK);
		} else if ((msg->id == ST_L_WINK_OFF && !(winkerState & (1<<MPWST_RIGHT))) || (msg->id == ST_R_WINK_OFF && winkerState & (1<<MPWST_RIGHT))) {
			winkerState &= ~(1<<MPWST_WINK);
		}
	}
}