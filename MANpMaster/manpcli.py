#
# MANp CLI
#

import sys
import array
import serial

port = int(sys.argv[1])
dataId = sys.argv[2]
if (len(sys.argv) > 3):
    dataVal = int(sys.argv[3])
else:
    dataVal = 0

idMap = {
    "ST_L_WINK_ON" : ["\x01", 0],
    "ST_L_WINK_OFF" : ["\x02", 0],
    "ST_R_WINK_ON" : ["\x03", 0],
    "ST_R_WINK_OFF" : ["\x04", 0],

    "VAR_WINK_INTENSITY" : ["\x81", 1],
    "VAR_WINK_FREQ" : ["\x82", 1],
    "VAR_WINK_DC" : ["\x83", 1],

    "ST_TAIL_ON" : ["\x05", 0],
    "ST_TAIL_BRAKE" : ["\x06", 0],
    "ST_TAIL_OFF" : ["\x07", 0],

    "VAR_TAIL_ON_INTENSITY" : ["\x84", 1],
    "VAR_TAIL_BRAKE_INTENSITY" : ["\x85", 1]
}

print "Data ID: %s" % dataId
print "Value: %i" % dataVal

ser = serial.Serial(port, 9600);
print "Writing to port: %s" % ser.portstr

if (dataId in idMap):
    ser.write(idMap[dataId][0])

    if (idMap[dataId][1] == 1):
        ser.write(array.array('B', [dataVal]).tostring())
else:
    print "No such dataId: " + dataId

ser.close()