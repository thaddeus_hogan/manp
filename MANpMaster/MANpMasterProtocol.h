/*
 * MANpMasterProtocol.h
 *
 * Created: 10/3/2012 5:17:19 PM
 *  Author: thogan
 */

#include "MANpProtocolData.h"

#ifndef MPPORT
#define MPPORT PORTA
#endif

#ifndef MPDDR
#define MPDDR DDRA
#endif

#ifndef MPPIN
#define MPPIN PINA
#endif

#ifndef MPBIT
#define MPBIT 0
#endif

#ifndef MANPMASTERPROTOCOL_H_
#define MANPMASTERPROTOCOL_H_

#define MP_POST_SEND_WAIT_US 300

// 010 == CLK/8
#define BTIM_CS 0b010
// #define BTIM_CS 0b101 // for debugging, very long bit width
// Match = x/((1/12)*CLKDIV) where x = bit length in us and CLKDIV = prescaler divisor (8 typically)
#define BTIM_MATCH 120

#define MP_VAR_MASK 0x80

#define MP_HIGH MPPORT |= (1<<MPBIT)
#define MP_LOW MPPORT &= ~(1<<MPBIT)

// Configure MANP port
void manpConfigure(void);

// Send a MANp Message to the network
void manpSend(mpmsg* msg);

// Resets Timer0 prescaler and count
void resetBitTimer(uint8_t timerRate);

// Called when send is complete to clean up
void manpSendComplete(void);

#endif /* MANPMASTERPROTOCOL_H_ */