/*
 * MANpMaster.c
 * Moped Area Network master controller. Targeting ATMEGA644A
 *
 * Created: 9/28/2012 10:18:40 PM
 *  Author: Thaddeus Hogan <thaddeus@thogan.com>
 */ 

#define F_CPU 12000000UL
#define SERIAL_BAUD 9600
#define UBRR_VALUE ((F_CPU/(16UL * SERIAL_BAUD)) - 1)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "MANpMasterProtocol.h"
#include "MANpProtocolData.h"

#define ST_L_WINK 0
#define ST_R_WINK 1

uint8_t lightState = 0;

mpmsg msg;

int main(void)
{
	#ifdef MP_DEBUG_STK500
	DDRB |= (1<<DDB7);
	PORTB |= (1<<PB7);
	#endif
	
	// Configure serial
	UCSR0B |= (1<<TXEN0) | (1<<RXEN0); // Enable tx and rx on USART
	UCSR0C |= (3<<UCSZ00); // Set 8-bit data
	UBRR0H = 0xF & (UBRR_VALUE>>8); // Set UBRR high byte
	UBRR0L = UBRR_VALUE; // Set UBRR low byte
	
	// Configure MANp
	manpConfigure();
	
	// Enable interrupts
	sei();
	
	// Send test message
 	//msg.id = ST_L_WINK_ON;
 	//msg.val = 0b10101010;
	//manpSend(&msg);
	
    while(1) {
        while (!(UCSR0A & (1<<RXC0)));
		
		if (msg.id) {
			msg.val = UDR0;
			manpSend(&msg);
			msg.id = 0;
		} else {
			msg.id = UDR0;
			if (!(msg.id & 0x80)) {
				
				#ifdef MP_DEBUG_STK500
				if (msg.id == ST_L_WINK_ON) {
					PORTB &= ~(1<<PB7);
				} else if (msg.id == ST_L_WINK_OFF) {
					PORTB |= (1<<PB7);
				}
				#endif
				
				manpSend(&msg);
				msg.id = 0;
			}
		}		
    }
}