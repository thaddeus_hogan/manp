from Tkinter import *
import serial
import array

idMap = {
    "ST_L_WINK_ON" : ["\x01", 0],
    "ST_L_WINK_OFF" : ["\x02", 0],
    "ST_R_WINK_ON" : ["\x03", 0],
    "ST_R_WINK_OFF" : ["\x04", 0],

    "VAR_WINK_INTENSITY" : ["\x81", 1],
    "VAR_WINK_FREQ" : ["\x82", 1],
    "VAR_WINK_DC" : ["\x83", 1],

    "ST_TAIL_ON" : ["\x05", 0],
    "ST_TAIL_BRAKE" : ["\x06", 0],
    "ST_TAIL_OFF" : ["\x07", 0],

    "VAR_TAIL_ON_INTENSITY" : ["\x84", 1],
    "VAR_TAIL_BRAKE_INTENSITY" : ["\x85", 1]
}

class MANpGUI:
    def __init__(self, master):
        self.ser = False
        self.port = 2

        frame = Frame(master, padx=16, pady=16)
        frame.pack()

        self.lPort = Label(frame, text="COM Port (starting with 0): ")
        self.lPort.grid(row=0, column=0)
        self.tPort = Entry(frame, width=10)
        self.tPort.grid(row=0, column=1, padx=8)
        self.bOpen = Button(frame, text="Open", command=self.openPort)
        self.bOpen.grid(row=0, column=2)

        self.lStatus = Label(frame, text="Not connected", justify=LEFT)
        self.lStatus.grid(row=1, column=0, columnspan=3)

        self.bBrakeOn = Button(frame, text="Brake ON", command=lambda: self.sendState("ST_TAIL_BRAKE"))
        self.bBrakeOn.grid(row=2, column=0, padx=8)
        self.bBrakeOff = Button(frame, text="Brake OFF", command=lambda: self.sendState("ST_TAIL_ON"))
        self.bBrakeOff.grid(row=2, column=1, padx=8)

        self.bLeftWinkOn = Button(frame, text="Left Wink ON", command=lambda: self.sendState("ST_L_WINK_ON"))
        self.bLeftWinkOn.grid(row=3, column=0, padx=8)
        self.bLeftWinkOff = Button(frame, text="Left Wink OFF", command=lambda: self.sendState("ST_L_WINK_OFF"))
        self.bLeftWinkOff.grid(row=3, column=1, padx=8)

        self.bRightWinkOn = Button(frame, text="Right Wink ON", command=lambda: self.sendState("ST_R_WINK_ON"))
        self.bRightWinkOn.grid(row=4, column=0, padx=8)
        self.bRightWinkOff = Button(frame, text="Right Wink OFF", command=lambda: self.sendState("ST_R_WINK_OFF"))
        self.bRightWinkOff.grid(row=4, column=1, padx=8)

        self.lWinkIntensity = Label(frame, text="Wink Intensity: ")
        self.lWinkIntensity.grid(row=5, column=0)
        self.tWinkIntensity = Entry(frame, width=15)
        self.tWinkIntensity.grid(row=5, column=1, padx=8)
        self.bWinkIntensity = Button(frame, text="Set", command=lambda: self.sendVar("VAR_WINK_INTENSITY", self.tWinkIntensity.get()))
        self.bWinkIntensity.grid(row=5, column=2)

        self.lWinkDC = Label(frame, text="Wink Duty Cycle: ")
        self.lWinkDC.grid(row=6, column=0)
        self.tWinkDC = Entry(frame, width=15)
        self.tWinkDC.grid(row=6, column=1, padx=8)
        self.bWinkDC = Button(frame, text="Set", command=lambda: self.sendVar("VAR_WINK_DC", self.tWinkDC.get()))
        self.bWinkDC.grid(row=6, column=2)

        self.lWinkFreq = Label(frame, text="Wink Frequency: ")
        self.lWinkFreq.grid(row=7, column=0)
        self.tWinkFreq = Entry(frame, width=15)
        self.tWinkFreq.grid(row=7, column=1, padx=8)
        self.bWinkFreq = Button(frame, text="Set", command=lambda: self.sendVar("VAR_WINK_FREQ", self.tWinkFreq.get()))
        self.bWinkFreq.grid(row=7, column=2)

        self.lTailIntensity = Label(frame, text="Tail Intensity: ")
        self.lTailIntensity.grid(row=8, column=0)
        self.tTailIntensity = Entry(frame, width=15)
        self.tTailIntensity.grid(row=8, column=1, padx=8)
        self.bTailIntensity = Button(frame, text="Set", command=lambda: self.sendVar("VAR_TAIL_ON_INTENSITY", self.tTailIntensity.get()))
        self.bTailIntensity.grid(row=8, column=2)

        self.lBrakeIntensity = Label(frame, text="Brake Intensity: ")
        self.lBrakeIntensity.grid(row=9, column=0)
        self.tBrakeIntensity = Entry(frame, width=15)
        self.tBrakeIntensity.grid(row=9, column=1, padx=8)
        self.bBrakeIntensity = Button(frame, text="Set", command=lambda: self.sendVar("VAR_TAIL_BRAKE_INTENSITY", self.tBrakeIntensity.get()))
        self.bBrakeIntensity.grid(row=9, column=2)

    def openPort(self):
        self.port = int(self.tPort.get())
        self.ser = serial.Serial(self.port, 9600);
        self.lStatus.config(text="Connected to port %s" % self.ser.portstr)

    def sendState(self, stateStr):
        self.ser.write(idMap[stateStr][0])

    def sendVar(self, stateStr, value):
        self.ser.write(idMap[stateStr][0])
        self.ser.write(array.array('B', [int(value)]).tostring())

root = Tk()
manpgui = MANpGUI(root)
root.mainloop()

if (manpgui.ser):
    print "Closing COM port"
    manpgui.ser.close()