/*
 * MANpMasterProtocol.c
 *
 * Master protocol handler. Provides functions for transmitting states and variables.
 * Requires target to run with a CPU clock of 12 MHz. An accurate clock is required
 * to ensure successful transmissions. All slave devices must run with the same clock
 * speed as the master.
 *
 * Created: 9/28/2012 10:22:56 PM
 *  Author: Thaddeus Hogan <thaddeus@thogan.com>
 */ 

#define F_CPU 12000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>

#include "MANpMasterProtocol.h"
#include "MANpProtocolData.h"

#define BTIM_STOP TCCR0B &= ~(0b111<<CS00)
#define BTIM_START TCCR0B |= (BTIM_CS<<CS00)
#define BTIM_RESET BTIM_STOP; GTCCR |= (1<<PSRSYNC); TCNT0 = 0; asm("nop"); BTIM_START
#define BTIM_NO_FLAG (!(TIFR0 & (1<<OCF0A)))
#define BTIM_RESET_FLAG TIFR0 |= (1<<OCF0A);

uint8_t volatile manpSendBit;
uint8_t volatile manpSendState;
mpmsg* _msg;

// Configure MANP port
void manpConfigure(void) {
	// Configure data line as output and drive high
	MPDDR |= (1<<MPBIT);
	MP_HIGH;
	
	// Configure Timer0 for bit timing
	TCCR0A |= (0b10<<WGM00); // Clear on match mode
	OCR0A = BTIM_MATCH; // Match on bit length
	
	/*
	 * Slaves will wait for 250ms after boot to ensure that the
	 * data line has been pulled high by the controller. Wait 500ms
	 * to ensure that the slaves are all started and ready.
	 */
	_delay_ms(500);
	
	#ifdef MP_DEBUG_STK500
	// Turn on LED0 on STK500 to indicate configuration completed
	DDRB |= (1<<DDB0);
	PORTB &= ~(1<<PB0);
	#endif
}

// Send a MANp Message to the network
void manpSend(mpmsg* msg) {
	// Pull low for 1 bit length to alert the slaves
	MP_LOW;
	BTIM_RESET;
	BTIM_RESET_FLAG;
	
	// Wait a bit length
	while (BTIM_NO_FLAG);
	BTIM_RESET_FLAG;
	
	// Send the data ID, MSB first
	for (uint8_t i = 7; i != 255; i--) {
		if (msg->id & (1<<i)) {
			MP_HIGH;
		} else {
			MP_LOW;
		}
		
		while (BTIM_NO_FLAG); // Wait a bit
		BTIM_RESET_FLAG;
	}
	
	// Send value if applicable
	if (msg->id & 0x80) {
		for (uint8_t i = 7; i != 255; i--) {
			if (msg->val & (1<<i)) {
				MP_HIGH;
			} else {
				MP_LOW;
			}
			
			while (BTIM_NO_FLAG); // Wait a bit
			BTIM_RESET_FLAG;
		}
	}
	
	// End on a high note
	MP_HIGH;
	
	// Wait slaves to process sent message
	_delay_us(MP_POST_SEND_WAIT_US);
}