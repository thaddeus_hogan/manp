/*
 * MANpTail.c
 *
 * Created: 10/3/2012 8:33:44 PM
 *  Author: thogan
 */ 

#define F_CPU 12000000UL

#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#include "MANpTail.h"
#include "MANpProtocolData.h"
#include "MANpSlaveProtocol.h"

#define PWM_ON TCCR0A |= (2<<COM0B0); TCCR0B |= (3<<CS00)
#define PWM_OFF TCCR0A &= ~(2<<COM0B0); TCCR0B &= ~(3<<CS00)

uint8_t volatile tailOnIntensity;
uint8_t volatile tailBrakeIntensity;

#define TS_EEPU 0
#define TS_BRAKE 1

uint8_t volatile tailState = 0;

int main(void)
{
	// To load chip with no settings
	//_delay_ms(1000);
	//eeprom_write_byte(NV_VAR_TAIL_ON_INTENSITY, 15);
	//eeprom_write_byte(NV_VAR_TAIL_BRAKE_INTENSITY, 255);
	
	// Load settings from EEPROM
	tailOnIntensity = eeprom_read_byte(NV_VAR_TAIL_ON_INTENSITY);
	tailBrakeIntensity = eeprom_read_byte(NV_VAR_TAIL_BRAKE_INTENSITY);
	
	// Configure the tail light
	tailConfigure();
	
	// Turn on tail light
	PWM_ON;
	
    while(1) {
		while(BTIM_NO_FLAG) {
			if (!(MPPIN & (1<<MPBIT))) {
				mpmsg* msg = manpReceiveMessage();
				onMessage(msg);
			}
		}
		
		BTIM_RESET_FLAG;
		
		if (tailState * (1<<TS_EEPU)) {
			tailState &= ~(1<<TS_EEPU);
			eeprom_update_byte(NV_VAR_TAIL_ON_INTENSITY, tailOnIntensity);
			eeprom_update_byte(NV_VAR_TAIL_BRAKE_INTENSITY, tailBrakeIntensity);
		}
	}
}

void tailConfigure(void) {
	// Wait for master to start
	_delay_ms(250);
	
	// PB2 - Data Input
	MPPORT &= ~(1<<MPBIT);
	MPDDR &= ~(1<<MPBIT);
	
	// PB1 - PWM Output to emitter FET
	DDRB |= (1<<PB1);
	
	TCCR0A |= (3<<WGM00);
	OCR0B = tailOnIntensity;
	
	// Configure MANp protocol
	manpConfigTimer1_ATtiny85();
	BTIM_RESET;
	BTIM_RESET_FLAG;
}

void onMessage(mpmsg* msg) {
	if (msg->id & 0x80) {
		// Variable write
		if (msg->id == VAR_TAIL_ON_INTENSITY) {
			tailOnIntensity = msg->val;
			if (!(tailState & (1<<TS_BRAKE))) {
				OCR0B = tailOnIntensity;
			}
		} else if (msg->id == VAR_TAIL_BRAKE_INTENSITY) {
			tailBrakeIntensity = msg->val;
			if (tailState & (1<<TS_BRAKE)) {
				OCR0B = tailBrakeIntensity;
			}
		}
		
		// Flag to write new values to EEPROM
		tailState |= (1<<TS_EEPU);
	} else {
		// Stage Message
		if (msg->id == ST_TAIL_ON) {
			OCR0B = tailOnIntensity;
		} else if (msg->id == ST_TAIL_BRAKE) {
			OCR0B = tailBrakeIntensity;
		}
	}
}