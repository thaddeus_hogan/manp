/*
 * MANpTail.h
 *
 * Created: 10/3/2012 9:02:42 PM
 *  Author: thogan
 */ 

#include "MANpProtocolData.h"

#ifndef MANPTAIL_H_
#define MANPTAIL_H_

#define NV_VAR_TAIL_ON_INTENSITY (uint8_t*)0x01
#define NV_VAR_TAIL_BRAKE_INTENSITY (uint8_t*)0x02

void tailConfigure(void);

void onMessage(mpmsg* msg);

#endif /* MANPTAIL_H_ */