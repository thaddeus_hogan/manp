/*
 * MANpSlaveProtocol.c
 *
 * Created: 9/29/2012 12:05:54 PM
 *  Author: thogan
 */ 

#define F_CPU 12000000UL

#include <avr/io.h>
#include <util/delay.h>

#include "MANpProtocolData.h"
#include "MANpSlaveProtocol.h"

// Storage for data line sampling
uint8_t volatile bitLowReads[16];
uint8_t volatile bitHighReads[16];

mpmsg message;

// Receive a message presuming the data line has just been detected going low
mpmsg* manpReceiveMessage(void) {
	// Reset the timer
	BTIM_RESET;
	BTIM_RESET_FLAG;
	
	// Zero the sampling storage buffer
	for (uint8_t i = 0; i < 16; i++) {
		bitLowReads[i] = 0;
		bitHighReads[i] = 0;
	}
	
	// Wait for a bit length, data starts 1 bit after the line goes low
	while (BTIM_NO_FLAG);
	BTIM_RESET_FLAG;
	
	// Begin sampling for id
	for (uint8_t i = 7; i != 255; i--) {
		while (BTIM_NO_FLAG) {
			if (MPPIN & (1<<MPBIT)) {
				bitHighReads[i] += 1;
			} else {
				bitLowReads[i] += 1;
			}
		}
		
		BTIM_RESET_FLAG;
	}
	
	// Quickly determine if first bit was 1, indicating a value to follow
	if (bitHighReads[7] > bitLowReads[7]) {
		// Begin sampling for val
		for (uint8_t i = 15; i != 7; i--) {
			while (BTIM_NO_FLAG) {
				if (MPPIN & (1<<MPBIT)) {
					bitHighReads[i] += 1;
				} else {
					bitLowReads[i] += 1;
				}
			}
			
			BTIM_RESET_FLAG;
		}
	}
	
	// Integrate the samples to decode the received waveform
	// Start with id
	message.id = 0;
	
	for (uint8_t i = 7; i != 255; i--) {
		if (bitHighReads[i] > bitLowReads[i]) {
			message.id |= (1<<i);
		}
	}
	
	// Decode the value if needed
	if (message.id & MP_VAR_MASK) {
		message.val = 0;
		
		for (uint8_t i = 7; i != 255; i--) {
			if (bitHighReads[i + 8] > bitLowReads[i + 8]) {
				message.val |= (1<<i);
			}
		}
	}
	
	return &message;
}

// Configure Timer for 80us interrupts, Timer1 - ATTiny85
void manpConfigTimer1_ATtiny85(void) {
	TCCR1 |= (1<<CTC1); // Clear on match of OCR1C
	OCR1A = BTIM_MATCH; // Match for interrupt at 80us
	OCR1C = BTIM_MATCH; // Match for reset at 80us
}