/*
 * MANpSlaveProtocol.h
 *
 * Created: 9/29/2012 12:06:15 PM
 *  Author: thogan
 */

#include "MANpProtocolData.h"

#ifndef MPPORT
#define MPPORT PORTB
#endif

#ifndef MPDDR
#define MPDDR DDRB
#endif

#ifndef MPPIN
#define MPPIN PINB
#endif

#ifndef MPBIT
#define MPBIT 2
#endif

#ifndef MANPSLAVEPROTOCOL_H_
#define MANPSLAVEPROTOCOL_H_

#define BTIM_CS 0b0100
// Match = x/((1/12)*CLKDIV) where x = bit length in us and CLKDIV = prescaler divisor (8 typically)
#define BTIM_MATCH 120

// BTIM stuff is target dependent, should clean this up and make it portable
#define BTIM_STOP TCCR1 &= ~(0b1111<<CS10)
#define BTIM_START TCCR1 |= (BTIM_CS<<CS10)
#define BTIM_RESET BTIM_STOP; GTCCR |= (1<<PSR1); TCNT1 = 0; asm("nop"); BTIM_START
#define BTIM_NO_FLAG (!(TIFR & (1<<OCF1A)))
#define BTIM_RESET_FLAG TIFR |= (1<<OCF1A)

#define MP_VAR_MASK 0x80

// Receive a message presuming the data line has just been detected going low
mpmsg* manpReceiveMessage(void);

/*
 * DEVICE SPECIFIC INITIALIZATION
 */

// Configure Timer for whole bit interrupts, Timer1 - ATTiny85
void manpConfigTimer1_ATtiny85(void);

#endif /* MANPSLAVEPROTOCOL_H_ */