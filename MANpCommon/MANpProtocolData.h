/*
 * MANpProtocolData.h
 *
 * Created: 9/29/2012 10:47:13 AM
 *  Author: thogan
 */ 


#ifndef MANPPROTOCOLDATA_H_
#define MANPPROTOCOLDATA_H_

// MANp Message Type
typedef struct {
	uint8_t id;
	uint8_t val;
} mpmsg;

// Winker States
#define ST_L_WINK_ON 0x01
#define ST_L_WINK_OFF 0x02
#define ST_R_WINK_ON 0x03
#define ST_R_WINK_OFF 0x04

// Winker Variables
#define VAR_WINK_INTENSITY 0x81
#define VAR_WINK_FREQ 0x82
#define VAR_WINK_DC 0x83

// Tail Light States
#define ST_TAIL_ON 0x05
#define ST_TAIL_BRAKE 0x06
#define ST_TAIL_OFF 0x07

// Tail Light Variables
#define VAR_TAIL_ON_INTENSITY 0x84
#define VAR_TAIL_BRAKE_INTENSITY 0x85

#endif /* MANPPROTOCOLDATA_H_ */